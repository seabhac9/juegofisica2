﻿using UnityEngine;
using System.Collections;

public class Nivel1 : MonoBehaviour {

	public bool isMision;
	public bool isRegresar;
	public bool isJugar;

	void OnMouseUp()
	{
		if(isMision)
		{
			Application.LoadLevel(3);
		}
		if (isRegresar)
		{
			Application.LoadLevel(2);
		}
		if (isJugar)
		{
			Application.LoadLevel(4);
		}
	}
}
