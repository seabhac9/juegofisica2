﻿using UnityEngine;
using System.Collections;

public class Nivel5 : MonoBehaviour {

	public bool isMision;
	public bool isRegresar;
	public bool isJugar;

	void OnMouseUp()
	{
		if(isMision)
		{
			Application.LoadLevel(15);
		}
		if (isRegresar)
		{
			Application.LoadLevel(14);
		}
		if (isJugar)
		{
			Application.LoadLevel(16);
		}
	}
}
