﻿using UnityEngine;
using System.Collections;

public class Nivel2 : MonoBehaviour {

	public bool isMision;
	public bool isRegresar;
	public bool isJugar;

	void OnMouseUp()
	{
		if(isMision)
		{
			Application.LoadLevel(6);
		}
		if (isRegresar)
		{
			Application.LoadLevel(5);
		}
		if (isJugar)
		{
			Application.LoadLevel(7);
		}
	}
}
