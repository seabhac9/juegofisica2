﻿using UnityEngine;
using System.Collections;

public class Nivel3 : MonoBehaviour {

	public bool isMision;
	public bool isRegresar;
	public bool isJugar;

	void OnMouseUp()
	{
		if(isMision)
		{
			Application.LoadLevel(9);
		}
		if (isRegresar)
		{
			Application.LoadLevel(8);
		}
		if (isJugar)
		{
			Application.LoadLevel(10);
		}
	}
}
