﻿using UnityEngine;
using System.Collections;

public class Nivel4 : MonoBehaviour {

	public bool isMision;
	public bool isRegresar;
	public bool isJugar;

	void OnMouseUp()
	{
		if(isMision)
		{
			Application.LoadLevel(12);
		}
		if (isRegresar)
		{
			Application.LoadLevel(11);
		}
		if (isJugar)
		{
			Application.LoadLevel(13);
		}
	}
}
