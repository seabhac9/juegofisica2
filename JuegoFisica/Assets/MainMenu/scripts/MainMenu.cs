﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public bool isStart;
    public bool isCredits;
    public bool isRegresar;

    public void OnMouseUp()
    {
        if(isStart)
        {
            Application.LoadLevel(2);
        }
        if (isCredits)
        {
            Application.LoadLevel(1);
        }
        if (isRegresar)
        {
            Application.LoadLevel(0);
        }
    }
}
