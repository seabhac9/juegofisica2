﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIFillDistance : MonoBehaviour {

    public Rigidbody ship;
    public GameObject goalRing;

    private Image image;
    private float maxLenght = 47;
    private float iniLenght = -109;
    private float dist = 0;

    // Use this for initialization
    void Start () {
        image = GetComponent<Image>();
        dist = Vector3.Distance(goalRing.transform.position, ship.position);
    }

    // Update is called once per frame
    void Update() {
        float distNow = Vector3.Distance(goalRing.transform.position, ship.position);
        float porcentaje = 1 - (distNow / dist);
        Debug.Log("DIST: " + porcentaje);
        porcentaje = porcentaje * (156);

        //GameObject imagecursor = GameObject.Find("cursor");
        Vector3 temp = new Vector3(12, porcentaje + 45, 0);
        //imagecursor.transform.position = temp;
        image.transform.position = temp;
    }
}
