﻿using UnityEngine;
using System.Collections;

public class RotateSpaceObj : MonoBehaviour {

    public GameObject spaceObject;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        spaceObject.transform.Rotate(Vector3.up * Time.deltaTime * -15);
    }
}
