﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Aeroplane;

public class PassingRing : MonoBehaviour {

    //public static PassingRing instance;
    public AudioSource audioRing;
    
    //public Vector3 shipPosCheck;
    //public Quaternion shipRotCheck;
    
    //void OnTriggerEnter(Collider coll)
    //{
    //    Debug.Log(coll.name);
    //}
    void Start()
    {
        PlayerPrefs.DeleteAll();
        
        //instance = this;
    }

    void OnTriggerExit(Collider coll)
    {
        if(coll.name == "BodyCentreCapsule")
        {
            int scoreValue = PlayerPrefs.GetInt("score");
            scoreValue++;
            PlayerPrefs.SetInt("score", scoreValue);
            
            Debug.Log("Anillos cruzados: " + scoreValue);

            GameObject nave = GameObject.Find("AircraftJet");
            AeroplaneController aeroplane = nave.GetComponent<AeroplaneController>();
            aeroplane.MaxEnginePower += 40;
            audioRing.Play();

            GameObject tubo = GameObject.Find("Cylinder");
            TuboCollider tuboColl = tubo.GetComponent<TuboCollider>();
            tuboColl.navePos = aeroplane.transform.position;
            tuboColl.naveRotCheck = aeroplane.transform.rotation;
            //Destroy(gameObject);
        }
    }
}
