﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Aeroplane;

public class UIFillVelocity : MonoBehaviour {

    public Rigidbody objectToMeasure;
    public float maxVelocity = 500;

    private Image image;

	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        image.fillAmount = (objectToMeasure.velocity.magnitude / maxVelocity);
        //Debug.Log("vel!" + objectToMeasure.velocity.magnitude + " fill" + (objectToMeasure.velocity.magnitude / maxVelocity));
    }
}
