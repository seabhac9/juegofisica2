﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Aeroplane;

public class TuboCollider : MonoBehaviour {

    public float counterTime = 0;
    public Vector3 navePos;
    public Quaternion naveRotCheck;
    public GameObject explosion;
    public AudioSource audioExplode;

    private bool onExplode = false;
    private Vector3 scaleExplo;
    // Use this for initialization
    void Start () {
        scaleExplo = explosion.transform.localScale;
    }

    void FixedUpdate()
    {
        counterTime += Time.deltaTime;
        if (counterTime >= 3)
        {
        //    GameObject nave = GameObject.Find("AircraftJet");
        //    AeroplaneController aeroplane = nave.GetComponent<AeroplaneController>();
        //    navePos = aeroplane.transform.position;
        //    Debug.Log("Save possition!" + counterTime);
            counterTime = 0;
        }
        if(onExplode)
        {
            explosion.transform.localScale += new Vector3(1, 1, 1);
        }
    }

    // Update is called once per frame
    void OnCollisionStay(Collision collision)
    {
        //if(collision.relativeVelocity.magnitude > 2)
        if (counterTime >= 2)
        {
            GameObject nave = GameObject.Find("AircraftJet");
            AeroplaneController aeroplane = nave.GetComponent<AeroplaneController>();

            explosion.transform.position = aeroplane.transform.position;
            audioExplode.Play();
            aeroplane.EnginePower = 0;
            aeroplane.MaxEnginePower = 0;
            aeroplane.GetComponent<Rigidbody>().velocity = Vector3.zero;
            aeroplane.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            StartCoroutine(EsperaExplosion(aeroplane));

            //aeroplane.EnginePower = 40;
            //aeroplane.MaxEnginePower = 40;
            //aeroplane.transform.position = navePos;
            //aeroplane.transform.rotation = Quaternion.Euler(0, 0, 0);
            //aeroplane.GetComponent<Rigidbody>().velocity = Vector3.zero;
            //aeroplane.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            
        }
    }

    IEnumerator EsperaExplosion(AeroplaneController aeroplane)
    {
        yield return new WaitForSeconds(1);
        
        aeroplane.transform.position = navePos;
        aeroplane.transform.rotation = naveRotCheck;
        aeroplane.EnginePower = 40;
        aeroplane.MaxEnginePower = 40;
        explosion.transform.localScale = scaleExplo;
    }
}
