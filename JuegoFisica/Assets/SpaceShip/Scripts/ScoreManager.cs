﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Vehicles.Aeroplane;

public class ScoreManager : MonoBehaviour {

    public static int valScore;
    public bool isSpeed;
    public bool isRing;
    public float counterTime = 0;
    public ParticleSystem partLuz;

    private bool timer = false;
    private int indicatorRing = 0;
    Text speedText;

    void Start()
    {
        partLuz.gameObject.SetActive(false);
    }
        
    // Use this for initialization
    void Awake () {
        speedText = GetComponent<Text>();
        valScore = 0;
	}

    void FixedUpdate()
    {
        if (timer == true )
        {
            counterTime += Time.deltaTime;
        }
    }

    // Update is called once per frame
    void Update () {
        GameObject nave = GameObject.Find("AircraftJet");
        AeroplaneController aeroplane = nave.GetComponent<AeroplaneController>();

        if(isSpeed)
        {
            valScore = (int)aeroplane.EnginePower;
            speedText.text = "Velocidad: " + valScore;
        }
        else if(isRing)
        {
            valScore = PlayerPrefs.GetInt("score");
            speedText.text = "Anillos: " + valScore;

            if (valScore % 3 == 0 && valScore > indicatorRing)
            {
                partLuz.gameObject.SetActive(true);
                timer = true;
                indicatorRing = valScore;
            }
            else if(counterTime >= 3)
            {
                partLuz.gameObject.SetActive(false);
                timer = false;
                counterTime = 0;
            }
        }
        
    }
}
